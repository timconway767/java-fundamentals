package JavaLabsStuff;

public class TestInterfaces {

    public static void main(String[] args) {
        CurrentAccount currentAccount = new CurrentAccount();
        SavingsAccount savingsAccount = new SavingsAccount();
        HomeInsurance homeInsurance = new HomeInsurance(47.5,75.1,200.0);
        Detailable[] detailables = new Detailable[3];
        detailables[0] = currentAccount;
        detailables[1] = savingsAccount;
        detailables[2] = homeInsurance;

        for(Detailable d : detailables){
            System.out.println(d.getDetails());
        }


    }

}
