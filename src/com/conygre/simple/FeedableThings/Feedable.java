package com.conygre.simple.FeedableThings;

public interface Feedable {

    void feed();
}
