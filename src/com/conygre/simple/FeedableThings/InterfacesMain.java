package com.conygre.simple.FeedableThings;

public class InterfacesMain {

    public static void main(String[] args) {
        Feedable[] thingsWeCanFeed = new Feedable[5];

        Person horace = new Person("Horace");

        horace.setName("Horace O'Brien");

        Dog mutt = new Dog("Setter");

        thingsWeCanFeed[0] = horace;
        thingsWeCanFeed[1] = mutt;

        for(Feedable x : thingsWeCanFeed){
            if(x != null) {
                x.feed();
                ((Dog)(x)).bark();

            }
        }

    }


}
