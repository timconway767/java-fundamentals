package com.conygre.simple.FeedableThings;

public class Dog implements Feedable{

    private String breed;

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public Dog(String breed) {
        this.breed = breed;
    }

    @Override
    public void feed() {

        System.out.println("Feeding it dog food");

    }

    public void bark(){
        System.out.println("Dog is barking");
    }
}
