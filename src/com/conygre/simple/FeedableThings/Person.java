package com.conygre.simple.FeedableThings;

public class Person implements Feedable {

    private String name;

    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void feed(){
        System.out.println("Feed the person");
    }
}
